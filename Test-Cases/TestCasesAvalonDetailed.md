##### Healthy Food #####

Test cases about Healthy Food website

https://sheltered-badlands-59110.herokuapp.com/


### TEST CASES PUBLIC PART ###


# TC01
Test Title: Registration - Check with valid data - only required fields filled in

Priority: Prio 1

Precondition: 
1. Home page is opened
2. User is not registered

Narrative:
1. In order to use the social network
2. As a user
3. I want to register to the social network 

Steps to execute:
1. Click "Sign Up" button from home page menu
2. Type Username in Username field (data: veselinminev@mail.bg)
3. Type password in Password field (data: Vesko123@)
4. Type password in Confirm password field (data: Vesko123@)
5. Type first name in First name field (data: Veselin)
6. Type last name in Last name field (data: Minev)
7. Select visibility from Profile picture visibility drop-down menu (data: PUBLIC)
8. Click "Choose File" and select picture for upload (data: drinks.png)
9. Leave empty Age field
10. Leave empty About you field
11. Do not select nationality from Nationality drop-down menu 
12. Do not select gender from Gender drop-down menu 
13. Click "REGISTER" button

Expected result: 
1. "Success registration. Before you can login, you must active your account by follow link sent to your email address." message appears on the screen.
-----------------------------------------------------------------------------------------------------------------------


# TC02
Test Title: Registration - Check with invalid data - only required fields filled in - negative

Priority: Prio 1

Precondition: 
1. Home page is opened
2. User is not registered

Narrative:
1. In order to use the social network
2. As a user
3. I want to register to the social network 

Steps to execute:
1. Click "Sign Up" button from home page menu
2. Leave empty Username field 
3. Leave empty Password field 
4. Type password in Confirm password field (data: Ve) 
5. Leave empty First name field 
6. Leave empty Last name field 
7. Leave empty Age field
8. Leave empty About you field
9. Do not select nationality from Nationality drop-down menu 
10. Do not select gender from Gender drop-down menu 
11. Do not select visibility from Profile picture visibility drop-down menu 
12. Do not click "Choose File"
13. Click "REGISTER" button

Expected result: 
1. "Not valid email format." message appears on the screen
2. "Password should be at least 8 characters and not more than 40 characters and should contain a minimum 1 lower case letter, 1 upper case letter, 1 numeric character and a special character: [@,#,$,%,!]" message appears on the screen
3. "Password doesn't match!" message appears on the screen
5. "User's first name should be between 2 and 50 characters." message appears on the screen
5. "User's last name should be between 2 and 50 characters." message appears on the screen
6. "Visibility's type is required." message appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC03
Test Title: Registration - Check with empty Username - negative

Priority: Prio 2

Precondition: 
1. Home page is opened
2. User is not registered

Narrative:
1. In order to use the social network
2. As a user
3. I want to register to the social network 

Steps to execute:
1. Click "Sign Up" button from home page menu
2. Leave empty Username field 
3. Type password in Password field (data:Vesko123@)
4. Type password in Confirm password field (data:Vesko123@)
5. Type first name in First name field (data:Veselin)
6. Type last name in Last name field (data:Minev)
7. Select nationality from Nationality drop-down menu (data:NAURUAN)
8. Select gender from Gender drop-down menu (data: MALE)
9. Select visibility from Profile picture visibility drop-down menu (Data: PUBLIC)
10. Click "Choose File" and select picture for upload (data: drinks.png)
11. Click "REGISTER" button

Expected result: 
1. "Not valid email format." message appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC04
Test Title: Latest Posts - Check chronological order of public posts

Priority: Prio 2

Precondition: 
1. Home page is opened
2. User is not registered

Narrative:
1. In order to use the social network
2. As a user
3. I want to check chronological order of public posts 

Steps to execute:
1. Click "LATEST POST" button

Expected result: 
1. Newest posts are at the top of the "Recent Post" field
-----------------------------------------------------------------------------------------------------------------------


# TC05
Test Title: Public User Profiles - Check user information

Priority: Prio 1

Precondition: 
1. Home page is opened
2. User is not registered
3. User with name: Veselin Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan exists

Narrative:
1. In order to use the social network
2. As a user
3. I want to check user information

Steps to execute:
1. Click "USERS" button
2. Click "more" button
3. Select user name "Veselin Minev"

Expected result: 
1. User page with information (Veselin Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan) about selected user opens. 
-----------------------------------------------------------------------------------------------------------------------


# TC06
Test Title: Public User Profiles - Check user posts order

Priority: Prio 1

Precondition: 
1. Home page is opened
2. User is not registered
3. User with name: Veselin Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan and some already created posts exists

Narrative:
1. In order to use the social network
2. As a user
3. I want to check user posts order 

Steps to execute:
1. Click "USERS" button
2. Click "more" button
3. Select user name "Veselin Minev"

Expected result: 
1. Newest posts are at the top of the "Lately posts" field 
-----------------------------------------------------------------------------------------------------------------------


# TC07
Test Title: Login - Check with valid credentials

Priority: Prio 1

Precondition: 
1. Registered user (username : petkoivanov34@mail.bg password : Telerikacademy1@)
2. Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to be able to login to my profile

Steps to execute:
1. Click "Sign in" button
2. Type Username in Username field (data: petkoivanov34@mail.bg)
3. Type password in Password field (data: Telerikacademy1@)
4. Click "Login" button

Expected result: 
1. User is logged successfully in the website
-----------------------------------------------------------------------------------------------------------------------


# TC08
Test Title: Login - Check with invalid credentials all fields - negative

Priority : Prio 1

Precondition: 
1. Registered user (username : petkoivanov34@mail.bg password : Telerikacademy1@)
2. Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to be able to login to my profile

Steps to execute:
1. Click "Sign in" button
2. Type Username in Username field (data: petkoivanov34@mail.b)
3. Type password in Password field (data: Telerikacademy1)
4. Click "Login" button

Expected result:
1. User is receiving error message for wrong username and password
-----------------------------------------------------------------------------------------------------------------------


# TC09
Test Title: Login - Check with invalid username - negative

Priority : Prio 1

Precondition: 
1. Registered user (username : petkoivanov34@mail.bg password : Telerikacademy1@)
2. Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to be able to login to my profile

Steps to execute:
1. Click "Sign in" button
2. Type Username in Username field (data: petkoivanov34@mail.b)
3. Type password in Password field (data: Telerikacademy1@)
4. Click "Login" button

Expected result:
1. User is receiving error message for wrong username or password
-----------------------------------------------------------------------------------------------------------------------


# TC10
Test Title: Login - Check with invalid password - negative

Priority : Prio 1

Precondition: 
1. Registered user (username : petkoivanov34@mail.bg password : Telerikacademy1@)
2. Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to be able to login to my profile

Steps to execute:
1. Click "Sign in" button
2. Type Username in Username field (data: petkoivanov34@mail.bg)
3. Type password in Password field (data: Telerikacademy1)
4. Click "Login" button

Expected result:
1. User is receiving error message for wrong username or password
-----------------------------------------------------------------------------------------------------------------------


# TC11
Test Title: Forgot password - Check with valid email

Priority: Prio 1

Precodintion:
1. Registered user (email : petkoivanov34@mail.bg)
2. Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to be able to login successfully after recovering my password

Steps to execute : 
1. Click "Sign in" button
2. Click "Forgot Password" button
3. Type email in Email field (data: petkoivanov34@mail.bg)
4. Click "Send" button

Expected result : 
1. User is received email with link to recover his password
-----------------------------------------------------------------------------------------------------------------------


# TC12
Test Title: Forgot password - Check with invalid email

Priority: Prio 1

Precodintion:
1. Registered user (email : petkoivanov34@mail.bg)
2. Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to be able to login successfully after recovering my password

Steps to execute : 
1. Click "Sign in" button
2. Click "Forgot Password" button
3. Type email in Email field (data: petkoivanov34@mail.b)
4. Click "Send" button

Expected result : 
1. User is receiving error message "User with this email does not exist!" 
-----------------------------------------------------------------------------------------------------------------------


### TEST CASES PRIVATE PART ###


# TC13
Test Title: Private Home Page - Check "LOG OUT" button navigation

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to log out user account 

Steps to execute:
1. Click "LOG OUT" button

Expected result: 
1. User is logged out.
2. Login page is opened.
-----------------------------------------------------------------------------------------------------------------------


# TC14
Test Title: Profile Administration - Check user information

Priority: Prio 4

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User data is name: Veselin Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to check my user information 

Steps to execute:
1. Select "PROFILE" from "PROFILE" drop-down menu

Expected result: 
1. User with data (name: Veselin Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan) appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC15
Test Title: Edit Profile - Check changing all fields with valid data

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User data is Profile picture visibility: PUBLIC, First name: Veselin, Last name: Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan, About: TTT
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to change my profile data 

Steps to execute:
1. Select "PROFILE" from "PROFILE" drop-down menu
2. Click "Edit" button
3. Type first name in First name field (data:Veselina)
4. Type last name in Last name field (data:Mineva)
5. Type info in About field (data: RRR)
5. Select nationality from Nationality drop-down menu (data:NAMIBIAN)
6. Select gender from Gender drop-down menu (data: FEMALE)
7. Select visibility from Profile picture visibility drop-down menu (Data: CONNECTIONS)
8. Click "SAVE" button
9. Select "PROFILE" from "PROFILE" drop-down menu

Expected result: 
1. User with edited information (Profile picture visibility: CONNECTIONS, First name: Veselina, Last name: Mineva, Email:  veselinminev@mail.bg, Gender: female, Nationality: Namibian, About: RRR) is visible
-----------------------------------------------------------------------------------------------------------------------


# TC16
Test Title: Edit Profile - Check changing all fileds with invalid data - negative

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User data is Profile picture visibility: PUBLIC, First name: Veselin, Last name: Minev, Email:  veselinminev@mail.bg, Gender: male, Nationality: Nauruan, About: TTT
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to change my profile only with valid data

Steps to execute:
1. Select "PROFILE" from "PROFILE" drop-down menu
2. Click "Edit" button
3. Leave empty First name field (data:Veselina)
4. Leave empty Last name field (data:Mineva)
5. Leave empty Age field
6. Leave empty About you field
7. Do not select nationality from Nationality drop-down menu 
8. Do not select gender from Gender drop-down menu 
9. Do not select visibility from Profile picture visibility drop-down menu 
10. Do not click "Choose File"
11. Click "SAVE" button

Expected result: 
1. User profile is not edited
2. "User's first name should be between 2 and 50 characters." message appears on the screen
3. "User's last name should be between 2 and 50 characters." message appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC17
Test Title: Change Password - Check with valid data

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to change my password 

Steps to execute:
1. Select "EDIT" from "PROFILE" drop-down menu
2. Click "CHANGE PASSWORD" button
3. Type old password in Enter old password field (data: Vesko123@)
4. Type new password in Enter password field (data: Vesko123@@ )
5. Type new password in Confirm password field (data: Vesko123@@)
6. Click "SAVE" button
7. Click "LOGOUT" button
8. Type username (data: veselinminev1@mail.bg)
9. Type password (data: Vesko123@@)
10. Click "LOGIN" button

Expected result: 
1. User password is changed
2. User is logged in
2. User Home page opens 
-----------------------------------------------------------------------------------------------------------------------


# TC18
Test Title: Change Password - Check with invalid old password - negative

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to change my password only with valid data 

Steps to execute:
1. Select "EDIT" from "PROFILE" drop-down menu
2. Click "CHANGE PASSWORD" button
3. Type old password in Enter old password field (data: Vesko)
4. Type new password in Enter password field (data: Vesko123@@@ )
5. Type new password in Confirm password field (data: Vesko123@@@)
6. Click "SAVE" button

Expected result: 
1. "User's old password doesn't match!" message appears on the screen
2. User password is not changed
-----------------------------------------------------------------------------------------------------------------------


# TC19
Test Title: Change Password - Check with less than 8 characters Password - negative

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to change my password only with valid data 

Steps to execute:
1. Select "EDIT" from "PROFILE" drop-down menu
2. Click "CHANGE PASSWORD" button
3. Type old password in Enter old password field (data: Vesko123@)
4. Type new password in Enter password field (data: 1 )
5. Type new password in Confirm password field (data: 1)
6. Click "SAVE" button

Expected result: 
1. "Password should be at least 8 characters and not more than 40 characters and should contain a minimum 1 lower case letter, 1 upper case letter, 1 numeric character and a special character: [@,#,$,%,!]" appears on the screen
2. User password is not changed
-----------------------------------------------------------------------------------------------------------------------


# TC20
Test Title: Change Password - Check with more than 40 characters Password - negative

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to change my password only with valid data 

Steps to execute:
1. Select "EDIT" from "PROFILE" drop-down menu
2. Click "CHANGE PASSWORD" button
3. Type old password in Enter old password field (data: Vesko123@)
4. Type new password in Enter password field (data: Vesko123456789123456789123456789123456789)
5. Type new password in Confirm password field (data: Vesko123456789123456789123456789123456789)
6. Click "SAVE" button

Expected result: 
1. "Password should be at least 8 characters and not more than 40 characters and should contain a minimum 1 lower case letter, 1 upper case letter, 1 numeric character and a special character: [@,#,$,%,!]" appears on the screen
2. User password is not changed
-----------------------------------------------------------------------------------------------------------------------


# TC21
Test Title: User Interaction - Check Connection to regular user

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Other user is registered (data: Veselin Minev1)
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to connect to other user

Steps to execute:
1. Select "USERS" from "USERS" drop-down menu
2. Select user "Veselin Minev1"
3. Click "CONNECT" button
4. Click "LOG OUT" button
5. Type username (data: veselinminev1@mail.bg)
6. Type password (data:Vesko123@)
7. Click "LOGIN" button
8. Select "REQUESTS" from "USERS" drop-down menu
9. Select "Veselin Minev"
10. Click "CONFIRM" button
11. Select "FRIENDS" from "USERS" drop-down menu

Expected result: 
1. User Veselin Minev appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC22
Test Title: User Interaction - Check "DISCONNECT" button navigation

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User is friend with other user (data: Veselin Minev1)
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to disconnect from user

Steps to execute:
1. Select "USERS" from "USERS" drop-down menu
2. Select user "Veselin Minev1"
3. Click "DISCONNECT" button

Expected result: 
1. Users are disconnected
2. "CONNECT" button appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC23
Test Title: User Requests - Check confirmation 

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Other user has already sent connection request (data: Veselin Minev1)
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to confirm user request for connection

Steps to execute:
1. Select "REQUESTS" from "USERS" drop-down menu
2. Select "Veselin Minev1"
3. Click "CONFIRM" button
4. Select "FRIENDS" from "USERS" drop-down menu

Expected result: 
1. User Veselin Minev1 appears on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC24
Test Title: User Requests - Check rejection

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Other user has already sent connection request (data: Veselin Minev1)
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to reject user request for connection

Steps to execute:
1. Select "REQUESTS" from "USERS" drop-down menu
2. Select "Veselin Minev1"
3. Click "REJECT" button
4. Select "FRIENDS" from "USERS" drop-down menu

Expected result: 
1. User Veselin Minev1 does not appear on the screen
-----------------------------------------------------------------------------------------------------------------------


# TC25
Test Title: Create Post - Check public post creation

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to create public post 

Steps to execute:
1. Select "NEW POST" from "LATEST POSTS" drop-down menu
2. Select "PUBLIC" from Post visibility drop-down menu
3. Click "Choose File" and select picture for upload (data: drinks.png)
4. Type title in field (data: Public Test Post)
5. Click "SAVE" button
6. Select "All POSTS" from "LATEST POSTS" drop-down menu

Expected result: 
1. New post "Public Test Post" appears in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC26
Test Title: Create Post - Check connections post creation

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Other user is already friend (data: Veselin Minev1)
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to create connections post

Steps to execute:
1. Select "NEW POST" from "LATEST POSTS" drop-down menu
2. Select "CONNECTIONS" from Post visibility drop-down menu
3. Click "Choose File" and select picture for upload (data: drinks.png)
4. Type title in field (data: Connections Test Post)
5. Click "SAVE" button
6. Click "LOG OUT" button
7. Type username (data: veselinminev1@mail.bg)
8. Type password (data:Vesko123@)
9. Click "LOGIN" button
10. Select "MY FRIENDS POSTS" from "LATEST POSTS" drop-down menu

Expected result: 
1. Post "Connections Test Post" appears in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC27
Test Title: Comment Post - Check commenting a post created by other user

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Other user has created post (data: Public Post by Admin)
2. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to comment a post created by other user 

Steps to execute:
1. Select "LATEST POSTS"
2. Select "Public Post by Admin"
3. Type comment in "leave a Comment" field (data: Great)
4. Click "SEND"

Expected result: 
1. Comment "Great" by User appears in field "Comments"
-----------------------------------------------------------------------------------------------------------------------


# TC28
Test Title: Comment Post - Check if newest comments are shown along with the post

Priority: Prio 3

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Post "First post" with x comments and y likes is created by other user Petko Ivanov
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to see newest post comments 

Steps to execute:
1. Click "View Full Post" button to open "First post"

Expected result: 
1. Newest comment is at the top of "Comments" field. Other comments are under it
-----------------------------------------------------------------------------------------------------------------------


# TC29
Test Title: Comment Post - Check if "See all" button expands comments section 

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Post "First post" with x comments and y likes is created by other user Petko Ivanov
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to see all comments about a post 

Steps to execute:
1. Click "View Full Post" button to open "First post"
2. Click "See all" button

Expected result: 
1. All x comments appear on the screen in "Comments" field
-----------------------------------------------------------------------------------------------------------------------


# TC30
Test Title: Like Post - Like a post created by other user

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Post "First post" with x comments and y likes is created by other user Petko Ivanov
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to like post created by other user 

Steps to execute:
1. Click "View Full Post" button to open "First post"
2. Click "Like" button

Expected result: 
1. Number of post likes changes to y+1
-----------------------------------------------------------------------------------------------------------------------


# TC31
Test Title: Dislike Post - Check Dislike a post created by other user

Priority: Prio 2

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Post "First post" with x comments and y likes is created by other user Petko Ivanov
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to dislike post created by other user

Steps to execute:
1. Click "View Full Post" button to open "First post"
2. Click "Like" button
3. Click "Disilke" button

Expected result: 
1. Number of post likes drops from y to y-1
-----------------------------------------------------------------------------------------------------------------------


# TC32
Test Title: Public User Profiles - Check Search with a keyword partially matching several existing User Names

Priority: Prio 4

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. User Veselin Minev and user Veselin Minev1 are registered
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to search for other users with a keyword partially matching several existing User Names

Steps to execute:
1. Select "USERS" from "USERS" drop-down menu
2. Type "Veselin" in search field
3. Click "SEARCH"

Expected result: 
1. User Veselin Minev and user Veselin Minev1 appears on the screen in Users field
-----------------------------------------------------------------------------------------------------------------------


# TC33
Test Title: Latest Posts - Check Search with a keyword partially matching several existing public Post Names

Priority: Prio 4

Precondition: 
1. User is logged in ( email: veselinminev@mail.bg, password: Vesko123@ )
2. Posts "Public Post by User Veselin Minev" and "Public Post by Admin" are created
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to search for posts with a keyword partially matching several existing public Post Names

Steps to execute:
1. Select "ALL POSTS" from "LATEST POSTS" drop-down menu
2. Type "Public Post by" in search field
3. Click "SEARCH"

Expected result: 
1. Posts "Public Post by User Veselin Minev" and "Public Post by Admin" appears on the screen in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC34
Test Title: My Friends Posts - Check Sort by Date

Priority: Prio 3

Precondition: 
1. User is logged in ( email: veselinminev1@mail.bg, password: Vesko123@ )
2. Other user is already friend (data: Veselin Minev)
3. Other user has created several posts (newest is: "New Post")
4. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to sort my friend's posts by date

Steps to execute:
1. Select "MY FRIENDS POSTS" from "LATEST POSTS" drop-down menu
2. Click "Date" button from Sort field

Expected result: 
1. Post "New Post" appears on top of posts in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC35
Test Title: My Friends Posts - Check Sort by Likes

Priority: Prio 3

Precondition: 
1. User is logged in ( email: veselinminev1@mail.bg, password: Vesko123@ )
2. Other user is already friend (data: Veselin Minev)
3. Other user has created several posts ("Public Test Post" with x>0 likes, the others with 0 likes)
4. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to I want to sort my friend's posts by likes

Steps to execute:
1. Select "MY FRIENDS POSTS" from "LATEST POSTS" drop-down menu
2. Click "Likes" button from Sort field

Expected result: 
1. Post "Public Test Post" appears on top of posts in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC36
Test Title: My Friends Posts - Check Sort by Comments

Priority: Prio 3

Precondition: 
1. User is logged in ( email: veselinminev1@mail.bg, password: Vesko123@ )
2. Other user is already friend (data: Veselin Minev)
3. Other user has created 6 posts ("Public Post by User Veselin Minev" with x>0 comments, the others with 0 comments)
4. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to I want to sort my friend's posts by comments

Steps to execute:
1. Select "MY FRIENDS POSTS" from "LATEST POSTS" drop-down menu
2. Click "Comments" button from Sort field

Expected result: 
1. Post "Public Post by User Veselin Minev" appears on top of posts in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC37
Test Title: My Friends Posts - Check Category filter

Priority: Prio 3

Precondition: 
1. User is logged in ( email: veselinminev1@mail.bg, password: Vesko123@ )
2. Other user is already friend (data: Veselin Minev)
3. Other user has created 6 posts (post "cake" is category SPORT, "Public Post by User Veselin Minev" and "Connections Post by User Veselin Minev" are Test Post Category, all other posts have no category)
4. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to filter my friend's posts by category 

Steps to execute:
1. Select "MY FRIENDS POSTS" from "LATEST POSTS" drop-down menu
2. Select "SPORT" from Sort field

Expected result: 
1. Post "cake" appears on top of posts in Recent Post field
-----------------------------------------------------------------------------------------------------------------------


# TC38
Test Title: My Friends Post - Check chronological order of my firends posts

Priority: Prio 1

Precondition: 
1. User is logged in ( email: veselinminev1@mail.bg, password: Vesko123@ )
2. Other user is already friend (data: Veselin Minev)
3. Other user has created 6 posts (newest is: "Connections Test Post", the oldest is "Children Food")
4. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to see my friend's posts in chronological order 

Steps to execute:
1. Select "MY FRIENDS POSTS" from "LATEST POSTS" drop-down menu

Expected result: 
1. Post "Connections Test Post" appears on top of posts in Recent Post field.
2. Post "Children Food" appears in the bottom of posts in Recent Post field.
-----------------------------------------------------------------------------------------------------------------------


### TEST CASES ADMIN PART ###


# TC39
Title : Admin Edit Other User Profile - Check changing all fields with valid data

Prio : Prio 1

Precondition: 
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative :
1. In order to use the social network
2. As admin
3. I want to be able to edit other user's profile

Steps to execute : 
1. Click "Users > Users"
2. Select the desired profile for editing
3. Click "Edit" button
4. Change the data of all fields displayed on page with valid data
5. Click "Save" button

Expected result : 
1. Data on the selected user profile is changed successfully
-----------------------------------------------------------------------------------------------------------------------


# TC40
Title : Admin Delete User Profile - Check deletion user profile

Prio : Prio 1

Precondition: 
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative :
1. In order to use the social network
2. As admin
3. I want to be able to delete other user's profile

Steps to execute : 
1. Click "Users > Users"
2. Select the desired profile for deletion
3. Click "Delete" button
4. Confirm deletion of user by clicking "Yes" button

Expected result : 
1. Selected user is deleted successfully
-----------------------------------------------------------------------------------------------------------------------


# TC41
Title: Admin Edit Post - Check editing of post

Prio: Prio 1

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative : 
1. In order to use the social network
2. As admin
3. I want to be able to edit other user's post

Steps to execute:
1. Hover on "Settings"
2. Click "Posts"
3. Choose your desired post and click on it
4. Click "Edit" button
5. Fill the desired fields with the new data
6. Click "Save" button

Expected result : 
1. Post is edited successfully
-----------------------------------------------------------------------------------------------------------------------


# TC42
Title: Admin Delete Post - Check deletion of a post created by other user

Prio: Prio 1

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative : 
1. In order to use the social network
2. As admin
3. I want to be able to to delete other user's post

Steps to execute:
1. Hover on "Settings"
2. Click "Posts"
3. Choose your desired post and click on it
4. Click "Edit" button
5. Click "Delete" button
6. Confirm deletion by clicking "Yes" button

Expected result : 
1. Post is deleted successfully
-----------------------------------------------------------------------------------------------------------------------


# TC43
Title: Admin Edit comment - Check changing comment created by other user

Prio: Prio 1

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative : 
1. In order to use the social network
2. As admin
3. I want to be able to to edit comment

Steps to execute:
1. Hover on "Latest posts"
2. Click "All posts"
3. In the "Sort" section click "Comments" button
4. Choose your desired post with comments
5. Click "Edit" button of the desired comment
6. Fill the comment field with the new data
7. Click "Save" button

Expected result : 
1. Comment is edited successfully
-----------------------------------------------------------------------------------------------------------------------


# TC44
Title: Admin Delete comment - Check deleting comment created by other user

Prio: Prio 1

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative : 
1. In order to use the social network
2. As admin
3. I want to be able to to delete comment

Steps to execute:
1. Hover on "Latest posts"
2. Click "All posts"
3. In the "Sort" section click "Comments" button
4. Choose your desired post with comments
5. Click "Edit" button of the desired comment
6. Click "Delete" button
7. Confirm deletion by clicking "Yes" button

Expected result : 
1. Comment is deleted successfully
-----------------------------------------------------------------------------------------------------------------------


# TC45
Title: Admin Settings - Add category with valid data 

Prio: Prio 2

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative: 
1. In order to use the social network
2. As admin
3. I want to be able to add new post category

Steps to execute: 
1. Hover on "Settings
2. Click "Categories"
3. Click "Add" button
4. Click "Upload" and choose your deisred category picture
5. Fill category name
6. Click "Save"

Expected result:
1. New post category is added successfully
-----------------------------------------------------------------------------------------------------------------------


# TC46
Title: Admin Settings - Add category with invalid data - negative

Prio: Prio 3

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative: 
1. In order to use the social network
2. As admin
3. I want to be able to add new post category only with valid data

Steps to execute:
1. Hover on "Settings
2. Click "Categories"
3. Click "Add" button
4. Click "Upload" and choose your deisred category picture
5. In field "Category's name" write  (data: "!@#$%^&*()")
6. Click "Save" button

Expected result:
1. An error message for invalid category name is displayed
-----------------------------------------------------------------------------------------------------------------------


# TC47
Title: Admin Settings - Edit category with valid data

Prio: Prio 3

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative:
1. In order to use the social network
2. As admin
3. I want to be able to edit category's name and picture

Steps to execute:
1. Hover on "Settings
2. Click "Categories"
3. Click "Edit" button of the desired category
4. Click "Upload" and upload new picture
5. In field "Category's name" write the new one
6. Click "Save" button

Expected result:
1. Category is edited successfully
-----------------------------------------------------------------------------------------------------------------------


# TC48
Title: Admin Settings - Edit category with invalid data - negative

Prio: Prio 3

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative: 
1. In order to use the social network
2. As admin
3. I want to be able to edit existing post category only with valid data

Steps to execute:
1. Hover on "Settings
2. Click "Categories"
3. Click "Edit" button of the desired category
4. Click "Upload" and upload new picture
5. In field "Category's name" write (data: "!@#$%^&*()")
6. Click "Save" button

Expected result:
1. An error message for invalid post category name is displayed
-----------------------------------------------------------------------------------------------------------------------


# TC49
Title: Admin Settings - Delete category

Prio: Prio 1

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative : 
1. In order to use the social network
2. As admin
3. I want to be able to delete category

Steps to execute:
1. Hover on "Settings"
2. Click "Categories"
3. Click "Edit" button of the desired category
4. Click "Delete" button
5. Confirm deletion by clicking "Yes" button

Expected result : 
1. Category is deleted successfully
-----------------------------------------------------------------------------------------------------------------------


# TC50
Title: Admin Settings - Add Nationality with valid data

Prio: Prio 2

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative: 
1. In order to use the social network
2. As admin
3. I want to be able to add new profile nationality

Steps to execute: 
1. Hover on "Settings
2. Click "Nationalities"
3. Click "Add" button
4. In field "Nationality's name" write (data: "Nationality")
5. Click "Save" button

Expected result:
1. New profile nationality is added successfully
-----------------------------------------------------------------------------------------------------------------------


# TC51
Title: Admin Settings - Add Nationality with invalid data - negative

Prio: Prio 3

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative: 
1. In order to use the social network
2. As admin
3. I want to be able to add only valid profile nationality

Steps to execute: 
1. Hover on "Settings
2. Click "Nationalities"
3. Click "Add" button
4. In field "Nationality's name" write (data: "!@#$%^&*()")
5. Click "Save" button

Expected result:
1. An error message for invalid nationality name is displayed
-----------------------------------------------------------------------------------------------------------------------


# TC52
Title: Admin Settings - Edit Nationality with valid data

Prio: Prio 2

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative:
1. In order to use the social network
2. As admin
3. I want to be able to edit profile nationality's name

Steps to execute:
1. Hover on "Settings
2. Click "Nationalities"
3. Click "Edit" button of the desired nationality
4. In field "Nationality's name" write the new nationality name
5. Click "Save" button

Expected result:
1. Nationality is edited successfully
-----------------------------------------------------------------------------------------------------------------------


# TC53
Title: Admin Settings - Edit Nationality with invalid data - negative

Prio: Prio 2

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative:
1. In order to use the social network
2. As admin
3. I want to be able to edit nationality's name only with valid data

Steps to execute:
1. Hover on "Settings
2. Click "Nationalities"
3. Click "Edit" button of the desired nationality
4. In field "Nationality's name" write (data: "!@#$%^&*()")
5. Click "Save" button

Expected result:
1. An error message for invalid nationality name is displayed
-----------------------------------------------------------------------------------------------------------------------


# TC54
Title: Admin Settings - Delete Nationality

Prio: Prio 2

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative : 
1. In order to use the social network
2. As admin
3. I want to be able to delete profile nationality

Steps to execute:
1. Hover on "Settings"
2. Click "Nationalities"
3. Click "Edit" button of the desired nationality
4. Click "Delete" button
5. Confirm deletion by clicking "Yes" button

Expected result : 
1. Nationality is deleted successfully
-----------------------------------------------------------------------------------------------------------------------


# TC55
Title: Admin Settings Posts - Check Search with a keyword partially matching one connections non friend existing Post Name

Prio: Prio 3

Precondition:
1. Admin is logged in ( email : avalonaddmin22@gmail.com password : Adminadmin1@ )
2. Admin Home page is opened

Narrative: 
1. In order to use the social network
2. As admin
3. I want to be able to search for certain post from all posts

Steps to execute:
1. Hover on "Settings"
2. Click "Posts"
3. In search field fill the desired post name
4. Click "Search" button

Expected result:
1. The desired post is found



### TEST CASES END-TO-END ###

# End-to-End01
Test Title: Registartion and Login

Priority: Prio 1

Precondition: 
1. Home page is opened
2. User is not registered

Narrative:
1. In order to use the social network
2. As a user
3. I want to register to the social network 

Steps to execute:
1. Click "Sign Up" from home page menu
2. Type Username in Username field (data: veselinminev1@mail.bg)
3. Type password in Password field (data: Vesko123@)
4. Type password in Confirm password field (data: Vesko123@)
5. Type first name in First name field (data: Veselin)
6. Type last name in Last name field (data: Minev)
7. Select nationality from Nationality drop-down menu (data: NAURUAN)
8. Select gender from Gender drop-down menu (data: MALE)
9. Select visibility from Profile picture visibility drop-down menu (data: PUBLIC)
10. Click "Choose File" and select picture for upload (data: drinks.png)
11. Click "REGISTER" button
12. "Success registration. Before you can login, you must active your account by follow link sent to your email address." message appears on the screen.
13. Click "LOGIN" button
14. Open user mailbox (data: veselinminev1@mail.bg)
15. Follow Email confirmation
16. Follow link "here" to verify email address
17. Type username (data: veselinminev1@mail.bg)
18. Type password (data: Vesko123@)
19. Click "LOGIN" button

Expected result: 
1. User is logged in. 
2. User Profile page is opened.
-----------------

# End-to-End02

Test Title: User Interaction - Check Connection to regular user

Priority: Prio 1

Precondition: 
1. User is logged in    (data: Veselin Minev)
2. Other user is registered (data: Veselin Minev1)
3. User Home page is opened

Narrative:
1. In order to use the social network
2. As a user
3. I want to connect to other user

Steps to execute:
1. Select "USERS" from "USERS" drop-down menu
2. Select user "Veselin Minev1"
3. Click "CONNECT" button
4. Click "LOG OUT" button
5. Type username (data: veselinminev1@mail.bg)
6. Type password (data:Vesko123@)
7. Click "LOGIN" button
8. Select "REQUESTS" from "USERS" drop-down menu
9. Select "Veselin Minev"
10. Click "CONFIRM" button
11. Click "LOG OUT"
12. Type username (data: veselinminev@mail.bg)
13. Type password (data:Vesko123@)
14. Click "LOGIN" button
15. Select "FRIENDS" from "USERS" drop-down menu

Expected result: 
1. User Veselin Minev1 appears on the screen
-----------------
