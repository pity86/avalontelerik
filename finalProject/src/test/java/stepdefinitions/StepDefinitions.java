package stepdefinitions;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.jbehave.core.annotations.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import testCases.BaseTest;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();
    BaseTest baseTest = new BaseTest();
    NavigationPage navPage = new NavigationPage();
    LoginPage loginPage = new LoginPage();
    ProfileRegistrationEditPage profileRegistrationEditPage = new ProfileRegistrationEditPage();
    ChangePasswordPage changePasswordPage = new ChangePasswordPage();


    @BeforeStory
    public void beforeEach(){
        actions.loadBrowser("base.url");
    }


    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        actions.typeValueInField(value, field);
    }

    @Given("Hover $element")
    @When("Hover $element")
    @Then("Hover $element")
    public void hoverElement(String element){
        actions.hoverElement(element);
    }

    @Given("Wait for $element")
    @When("Wait for $element")
    @Then("Wait for $element")
    public void elementIsPresent(String element) {
        actions.isElementPresentUntilTimeout(element,6);
    }

    @Given("Wait")
    @When("Wait")
    @Then("Wait")
    public void waitFor() {
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }

    @Given("As a registered user I am logging with $username and $password")
    @When("As a registered user I am logging with $username and $password")
    @Then("As a registered user I am logging with $username and $password")
    public void loginToWebsite(String username, String password){
        loginPage.login(username, password);
    }

    @Given ("I open login page")
    public void openSignIn(){
        navPage.openLoginPageAfterSignIn();
    }


    @Given ("I open Profile overview page")
    @When ("I open Profile overview page")
    @Then ("I open Profile overview page")
    public void openMyProfile(){
        navPage.openMyProfileOverviewPage();
    }

    @Given ("I return to home page")
    @When ("I return to home page")
    @Then ("I return to home page")
    public void returnHomePage(){
        changePasswordPage.returnHomePage();
    }

    @Given ("I open Profile page")
    @When ("I open Profile page")
    @Then ("I open Profile page")
    public void openProfile(){
        navPage.openProfileEditPage();
    }

    @Given ("I open Change password page")
    @When ("I open Change password page")
    @Then ("I open Change password page")
    public void openChangePassword(){
        profileRegistrationEditPage.openChangePasswordPage();
    }

    @Given ("I fill wrong data in fields")
    @When ("I fill wrong data in fields")
    @Then ("I fill wrong data in fields")
    public void fillWrongChangePassword() {
        changePasswordPage.fillChangePasswordFieldsWrongData();
    }

    @Given ("I log out")
    @When ("I log out")
    @Then ("I log out")
    public void logOut() {
        navPage.openLoginPageAfterLogOut();
    }

    @Then("I assert presence on User's home page")
    public void userIsLogged(){
        actions.assertElementPresent("navigation.LogOut");
    }

    @Then("I assert Admin added new nationality")
    public void adminAddedNationality(){
        actions.assertElementPresent("nationality.NewNational");
    }

    @Then("I assert password is not changed and message appears that the old one is wrong")
    public void passMessageWrongOldOne(){
        actions.assertElementPresent("changePassword.WrongOldPasswordMessage");
    }

    @Then("I assert user profile is correct")
    public void correctUserProfile(){
        actions.assertElementPresent("email.VeselinMinev");
        actions.assertElementPresent("profileOverview.Email");
    }


}
