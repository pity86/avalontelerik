package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.*;
import org.junit.After;
import org.junit.Before;

public class BaseStepDefinitions {
    @BeforeStories
    public void beforeStories(){
        UserActions.loadBrowser("base.url");
    }

    @AfterStories
    public void afterStories(){
        UserActions.quitDriver();
    }
}
