package testCases;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangePasswordTest extends BaseTest {

    NavigationPage navPage = new NavigationPage();

    private ProfileRegistrationEditPage profileRegistrationEditPage;
    private ChangePasswordPage changePasswordPage;
    private LoginPage loginPage;

    @Before
    public void openNewBrowser() {
        UserActions.loadBrowser("base.url");
        loginPage = navPage.openLoginPageAfterSignIn();
        navPage = loginPage.login(Constants.USER_THREE_USERNAME, Constants.USER_THREE_PASSWORD);
    }

    @After
    public void closePage() {
        UserActions.quitDriver();
    }

    @Test
    public void ChangePassword_CheckWithValidData() {
        profileRegistrationEditPage = navPage.openProfileEditPage();
        changePasswordPage = profileRegistrationEditPage.openChangePasswordPage();
        changePasswordPage.fillChangePasswordFields();
        navPage.openLoginPageAfterLogOut();
        loginPage.login(Constants.USER_THREE_USERNAME, Constants.USER_THREE_UPDATED_PASSWORD);

        actions.assertElementPresent(navPage.logOutButton);
    }
}
