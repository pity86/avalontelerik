package testCases;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrivateTest extends BaseTest {

    NavigationPage navPage = new NavigationPage();
    CommonPage commonPage = new CommonPage();
    PostsPage postsPage = new PostsPage();
    PostOverviewPage postOverviewPage = new PostOverviewPage();
    LoginPage loginPage = new LoginPage();

    private UsersPage usersPage;
    private ProfileOverviewPage profileOverviewPage;
    private PostCreateEditPage postCreateEditPage;
    private ProfileRegistrationEditPage profileRegistrationEditPage;
    private ChangePasswordPage changePasswordPage;

    @Before
    public void setPrecondition() {
        UserActions.loadBrowser("base.url");
        loginPage = navPage.openLoginPageAfterSignIn();
        navPage = loginPage.login(Constants.USER_ONE_USERNAME, Constants.USER_PASSWORD);
    }

    @After
    public void closePage() {
        UserActions.quitDriver();
    }

    @Test
    public void UserInteraction_CheckConnectionToRegularUser() {
        usersPage = navPage.openUsersPage();
        commonPage.searchValue(Constants.USER_ONE_FIRST_NAME);
        profileOverviewPage = usersPage.openProfileOverviewPage(commonPage.regularUserTwo);
        profileOverviewPage.connectToUser();
        loginPage = navPage.openLoginPageAfterLogOut();
        navPage = loginPage.login(Constants.USER_TWO_USERNAME, Constants.USER_PASSWORD);
        usersPage = navPage.openRequestOverviewPage();
        profileOverviewPage = usersPage.openProfileOverviewPage(commonPage.regularUserOne);
        profileOverviewPage.confirmRequest();
        usersPage = navPage.openFriendsPage();
        profileOverviewPage = usersPage.openProfileOverviewPage(commonPage.regularUserOne);
        profileOverviewPage.disconnectUser();

        actions.assertElementPresent(profileOverviewPage.usersConnectButton);
    }

    @Test
    public void PublicUserProfiles_CheckSearchWithAKeywordPartiallyMatchingSeveralExistingUserNames() {
        usersPage = navPage.openUsersPage();
        commonPage.searchValue(Constants.USER_ONE_FIRST_NAME);

        actions.assertElementPresent(commonPage.regularUserOne);
        actions.assertElementPresent(commonPage.regularUserTwo);
    }

    @Test
    public void CreatePost_CheckPublicPostCreation() {
        postCreateEditPage = navPage.openPostCreatePage();
        postCreateEditPage.fillAndSaveAllRequiredPostFields();

        actions.assertElementPresent(commonPage.titlePostOne);
    }

    @Test
    public void ChangePassword_CheckWithInvalidOldPassword() {
        profileRegistrationEditPage = navPage.openProfileEditPage();
        changePasswordPage = profileRegistrationEditPage.openChangePasswordPage();
        changePasswordPage.fillChangePasswordFieldsWrongData();

        actions.assertElementPresent(changePasswordPage.changePasswordWrongOldPasswordMessage);
    }

    @Test
    public void PublicUserProfiles_CheckUserInformation() {
        usersPage = navPage.openUsersPage();
        usersPage.findUser(commonPage.regularUserOne);
        profileOverviewPage = usersPage.openProfileOverviewPage(commonPage.regularUserOne);

        actions.assertElementPresent(commonPage.emailVeselinMinev);
        actions.assertElementPresent(commonPage.genderVeselinMinev);
        actions.assertElementPresent(commonPage.nationalityVeselinMinev);
        actions.assertElementPresent(commonPage.profileOverviewEmail);
        actions.assertElementPresent(commonPage.profileOverviewGender);
        actions.assertElementPresent(commonPage.profileOverviewNationality);
    }

    @Test
    public void CommentPost_CheckCommentingAPostCreatedByOtherUser() {
        postsPage = navPage.openPostsPage();
        commonPage.searchValue(Constants.PUBLIC_POST_CREATED_BY_ADMIN);
        postsPage.openPostOverviewPage(commonPage.publicPostAdmin);
        postOverviewPage.addNewComment(Constants.COMMENT_OF_PUBLIC_POST_CREATED_BY_ADMIN);

        actions.assertElementPresent(Constants.LOCATOR_TITLE_COMMENT_GREAT);
    }
}


