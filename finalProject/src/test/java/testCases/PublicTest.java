package testCases;

import com.telerikacademy.finalproject.pages.ForgotPasswordPage;
import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.ProfileRegistrationEditPage;
import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PublicTest extends BaseTest {

    NavigationPage navPage = new NavigationPage();

    private LoginPage loginPage;
    private ForgotPasswordPage forgotPasswordPage;
    private ProfileRegistrationEditPage profileRegistrationEditPage;

    @Before
    public void openNewBrowser() {
        UserActions.loadBrowser("base.url");
    }

    @After
    public void closePage() {
        UserActions.quitDriver();
    }

    @Test
    public void Registration_CheckWithValidData_OnlyRequiredFieldsFilledIn() {
        profileRegistrationEditPage = navPage.openRegistrationPage();
        profileRegistrationEditPage.fillInAllRequiredFields();

        actions.assertElementPresent(profileRegistrationEditPage.registerMessageSent);
    }

    @Test
    public void Registration_CheckWithInvalidData_OnlyRequiredFieldsFilledIn() {
        profileRegistrationEditPage = navPage.openRegistrationPage();
        profileRegistrationEditPage.fillInAllRequiredFieldsWrongData();

        actions.assertElementPresent(profileRegistrationEditPage.registerMessageWrongUsername);
        actions.assertElementPresent(profileRegistrationEditPage.registerMessageWrongPassword);
        actions.assertElementPresent(profileRegistrationEditPage.registerMessageWrongConfirmPassword);
        actions.assertElementPresent(profileRegistrationEditPage.registerMessageWrongFirstName);
        actions.assertElementPresent(profileRegistrationEditPage.registerMessageWrongLatsName);
        actions.assertElementPresent(profileRegistrationEditPage.registerMessageWrongPictureVisibility);
    }

    @Test
    public void ForgotPassword_CheckWithValidEmail() {
        loginPage = navPage.openLoginPageAfterSignIn();
        forgotPasswordPage = loginPage.openForgotPasswordPage();
        forgotPasswordPage.fillAndSendEmail(Constants.RANDOM_VALID_EMAIL);

        actions.assertElementPresent(forgotPasswordPage.forgotPassMessageSent);
    }

    @Test
    public void ForgotPassword_CheckWithInvalidEmail() {
        loginPage = navPage.openLoginPageAfterSignIn();
        forgotPasswordPage = loginPage.openForgotPasswordPage();
        forgotPasswordPage.fillAndSendEmail(actions.createRandomString(2));

        actions.assertElementPresent(forgotPasswordPage.forgotPassUsernameNotExist);
    }

    @Test
    public void Login_CheckWithValidCredentials() {
        loginPage = navPage.openLoginPageAfterSignIn();
        navPage = loginPage.login(Constants.USER_ONE_USERNAME, Constants.USER_PASSWORD);

        actions.assertElementPresent(navPage.logOutButton);
    }

    @Test
    public void Login_CheckWithInvalidCredentialsAllFields() {
        loginPage = navPage.openLoginPageAfterSignIn();
        navPage = loginPage.login(Constants.USER_ONE_LAST_NAME, Constants.USER_WRONG_PASSWORD);

        actions.assertElementPresent(loginPage.loginWrongUsernameOrPassword);
    }
}







