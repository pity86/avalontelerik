Meta:
@riki

Narrative:
As a user
I want to change my password in social network only with meeting requirements one
So that I can use it

Scenario: Change Password - Check with invalid data (less than 8 symbols)
Given I open login page
And As a registered user I am logging with veselinminev@mail.bg and Vesko123@
And I open Profile page
And I open Change password page
And I fill wrong data in fields
Then I assert password is not changed and message appears that the old one is wrong
And I return to home page
And I log out

