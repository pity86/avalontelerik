package com.telerikacademy.finalproject.utils;

public class Constants {

//    WAITS

    public static final int LONG_WAIT = 10;
    public static final int MEDIUM_WAIT = 6;
    public static final int SHORT_WAIT = 3;

//    ALL REGULAR USERS DATA

    public static final String USER_PASSWORD = "Vesko123@";
    public static final String USER_UPDATED_PASSWORD = "Vesko123@@";
    public static final String USER_WRONG_PASSWORD = "Ve";

    public static final String RANDOM_VALID_EMAIL = "todorkanikolova@mail.bg";

    public static final String USER_EDIT_PICTURE = "C:\\Users\\user1\\Desktop\\smiley_face.jpg";
    public static final String USER_EDIT_FIRST_NAME = "First Name";
    public static final String USER_EDIT_LAST_NAME = "Last Name";
    public static final String USER_EDIT_AGE = "46";
    public static final String USER_EDIT_ABOUT = "About";

//    USER ONE DATA

    public static final String USER_ONE_USERNAME = "veselinminev@mail.bg";
    public static final String USER_ONE_FIRST_NAME = "Veselin";
    public static final String USER_ONE_LAST_NAME = "Minev";
    public static final String USER_ONE_PROFILE_IMAGE = "D:\\AlphaQA22Telerik\\_FinalProject\\healthy-food-social-network\\images\\energy.png";

//    USER TWO DATA

    public static final String USER_TWO_USERNAME = "veselinminev1@mail.bg";
    public static final String USER_TWO_FIRST_NAME = "Veselin";
    public static final String USER_TWO_LAST_NAME = "Minev1";

//    USER THREE DATA

    public static final String USER_THREE_USERNAME = "todorkanikolova@mail.bg";
    public static final String USER_THREE_PASSWORD = "Vesko123@";
    public static final String USER_THREE_UPDATED_PASSWORD = "Vesko123@@";
    public static final String USER_THREE_FIRST_NAME = "Todorka";
    public static final String USER_THREE_LAST_NAME = "Nikolova";

//    ADMIN DATA

    public static final String ADMIN_USERNAME = "avalonaddmin22@gmail.com";
    public static final String ADMIN_PASSWORD = "Adminadmin1@";

//    POST COMMENT

    public static final String PUBLIC_POST_CREATED_BY_ADMIN = "Public post by admin";
    public static final String COMMENT_OF_PUBLIC_POST_CREATED_BY_ADMIN = "Great";
    public static final String LOCATOR_TITLE_COMMENT_GREAT = "titleCommentGreat";
    public static final String COMMENT_UPDATE = "comment update";

//    POST CREATED BY REGULAR USER

    public static final String POST_BY_USER_ONE_TITLE = "postOne";
    public static final String POST_BY_USER_ONE_IMAGE = "D:\\AlphaQA22Telerik\\_FinalProject\\healthy-food-social-network\\images\\energy.png";

//    POST CONSTANTS

    public static final String POST_EDIT_TITLE = "new title";
    public static final String POST_EDIT_DESCRIPTION = "new description";
    public static final String POST_EDIT_PICTURE = "C:\\Users\\user1\\Desktop\\smoothies.png";

//    NATIONALITIES CONSTANTS

    public static final String NATIONALITY_NAME = "New nationality";
    public static final String NATIONALITY_EDIT_NAME = "Afghannn";

//    CATEGORIES CONSTANTS

    public static final String CATEGORY_NEW_IMAGE = "C:\\Users\\user1\\Desktop\\drinks.png";
    public static final String CATEGORY_NEW_NAME = "Selenium Category";
    public static final String CATEGORY_EDIT_NAME = "Selenium Updated Category";
    public static final String CATEGORY_EDIT_PICTURE = "C:\\Users\\user1\\Desktop\\spices.png";

}

