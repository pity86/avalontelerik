package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class PostCreateEditPage extends BasePage {

    CommonPage commonPage = new CommonPage();
    ProfileRegistrationEditPage profileRegistrationEditPage = new ProfileRegistrationEditPage();

    public final String newPostVisibility = "newPost.Visibility";
    public final String newPostPictureChooseFile = "newPost.PictureChooseFile";
    public final String newPostTitle = "newPost.Title";
    public final String newPostDescription = "newPost.Description";

    public final String updatedTitle = "postTitleUpdated";
    public final String updatedDescription = "postDescriptionUpdated";
    public final String latestPost = "post.LatestPost";
    public final String postEditButton = "post.EditButton";
    public final String visibilityField = "edit.PictureVisibility";
    public final String pictureVisbilityType = "visibility.Connections";
    public final String picturePostUpload = "newPost.PictureChooseFile";
    public final String postCategory = "categories.firstCategory";
    public final String postTitle = "newPost.Title";
    public final String descriptionField = "newPost.Description";
    public final String saveButton = "save.Button";
    public final String deletePostButton = "delete.deletePostButton";

    public void fillAndSaveAllRequiredPostFields() {
        Utils.LOG.info("Filling and saving post data fields");
        actions.typeValueInField(Constants.POST_BY_USER_ONE_TITLE, newPostTitle);
        actions.clickElement(newPostVisibility);
        actions.clickElement(profileRegistrationEditPage.visibilityPublic);
        actions.uploadImage(Constants.POST_BY_USER_ONE_IMAGE, newPostPictureChooseFile);
        actions.clickElement(commonPage.saveButton);
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }

    public void editPost() {
        actions.implicitWait(Constants.MEDIUM_WAIT);
        actions.clickElement(latestPost);
        Utils.LOG.info("Clicking on latest post");
        actions.clickElement(postEditButton);
        Utils.LOG.info("Clicking on latest post edit button");
        actions.clickElement(visibilityField);
        Utils.LOG.info("Clicking on visiblity field");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(pictureVisbilityType);
        Utils.LOG.info("Changing visibility type");
        actions.uploadImage(Constants.POST_EDIT_PICTURE, picturePostUpload);
        Utils.LOG.info("Changing of existing image");
        actions.clickElement(postCategory);
        Utils.LOG.info("Changing post category");
        actions.clearField(postTitle);
        actions.typeValueInField(Constants.POST_EDIT_TITLE, postTitle);
        Utils.LOG.info("Changing post title");
        actions.clearField(descriptionField);
        actions.typeValueInField(Constants.POST_EDIT_DESCRIPTION, descriptionField);
        Utils.LOG.info("Changing post description");
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking save button");
        actions.assertElementPresent(updatedTitle);
        actions.assertElementPresent(updatedDescription);
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }

    public void deletePost() {
        actions.implicitWait(Constants.LONG_WAIT);
        actions.clickElement(latestPost);
        Utils.LOG.info("Clicking on latest post");
        actions.clickElement(postEditButton);
        Utils.LOG.info("Clicking on latest post edit button");
        actions.clickElement(deletePostButton);
        Utils.LOG.info("Clicking delete button");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking save button");
        actions.implicitWait(Constants.SHORT_WAIT);
    }
}
