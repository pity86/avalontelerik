package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class ProfileOverviewPage extends BasePage {

    public final String usersConnectButton = "users.ConnectButton";
    public final String userDisconnectButton = "users.DisconnectButton";

    public final String requestsUserConfirm = "requestsUser.Confirm";
    public final String requestsUserReject = "requestsUser.Reject";

    public final String updatedFirstName = "firstNameUpdated";
    public final String updatedLastName = "lastNameUpdated";
    public final String updatedAge = "ageUpdated";
    public final String updatedNationality = "nationalityUpdated";
    public final String updatedAbout = "aboutUpdated";

    public static final String selectedUser = "users.SelectedUser";
    public static final String editButton = "users.EditButton";
    public static final String visibilityField = "edit.PictureVisibility";
    public static final String pictureVisibilityType = "visibility.Connections";
    public static final String pictureProfileUpload = "edit.PictureChooseFile";
    public static final String firstName = "edit.FirstName";
    public static final String lastName = "edit.LastName";
    public static final String nationality = "edit.Nationality";
    public static final String bulgarian = "nationality.Bulgarian";
    public static final String genderField = "edit.Gender";
    public static final String femaleType = "gender.Male";
    public static final String ageField = "edit.Age";
    public static final String aboutField = "edit.About";
    public static final String saveButton = "save.Button";
    public static final String deleteButton = "delete.Button";

    public void connectToUser() {
        Utils.LOG.info("Sending connection request to user");
        actions.clickElement(usersConnectButton);
    }

    public void disconnectUser() {
        Utils.LOG.info("Disconnection request to user");
        actions.clickElement(userDisconnectButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    public void confirmRequest() {
        Utils.LOG.info("Confirming connection request");
        actions.clickElement(requestsUserConfirm);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    public void editOtherUserProfile() {
        actions.implicitWait(Constants.MEDIUM_WAIT);
        actions.clickElement(selectedUser);
        Utils.LOG.info("Clicking on the user to edit");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(editButton);
        Utils.LOG.info("Clicking edit button of the user");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(visibilityField);
        Utils.LOG.info("Clicking on visibility field");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(pictureVisibilityType);
        Utils.LOG.info("Changing visibility type");
        actions.uploadImage(Constants.USER_EDIT_PICTURE, pictureProfileUpload);
        Utils.LOG.info("Changing of existing image");
        actions.clearField(firstName);
        actions.typeValueInField(Constants.USER_EDIT_FIRST_NAME, firstName);
        Utils.LOG.info("Changing of first name of the user");
        actions.clearField(lastName);
        actions.typeValueInField(Constants.USER_EDIT_LAST_NAME, lastName);
        Utils.LOG.info("Changing of last name of the user");
        actions.clickElement(nationality);
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(bulgarian);
        Utils.LOG.info("Setting nationality of user to Bulgarian");
        actions.clickElement(genderField);
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(femaleType);
        Utils.LOG.info("Changing gender of the user");
        actions.clearField(ageField);
        actions.typeValueInField(Constants.USER_EDIT_AGE, ageField);
        Utils.LOG.info("Changing age of the user");
        actions.clearField(aboutField);
        actions.typeValueInField(Constants.USER_EDIT_ABOUT, aboutField);
        Utils.LOG.info("Changing about field of the user");
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking save button");
        actions.assertElementPresent(updatedFirstName);
        actions.assertElementPresent(updatedLastName);
        actions.assertElementPresent(updatedAge);
        actions.assertElementPresent(updatedNationality);
        actions.assertElementPresent(updatedAbout);
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }

    public void deleteUser() {
        actions.implicitWait(Constants.MEDIUM_WAIT);
        actions.clickElement(selectedUser);
        Utils.LOG.info("Clicking on the user to edit");
        actions.clickElement(editButton);
        Utils.LOG.info("Clicking edit button of the user");
        actions.clickElement(deleteButton);
        Utils.LOG.info("Clicking delete button of the user");
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking YES button to delete user");
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }
}
