package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class ProfileRegistrationEditPage extends BasePage {

    public final String editUsername = "edit.Username";
    public final String editPassword = "edit.Password";
    public final String editConfirmPassword = "edit.ConfirmPassword";
    public final String editFirstName = "edit.FirstName";
    public final String editLastName = "edit.LastName";

    public final String editPictureVisibility = "edit.PictureVisibility";
    public final String visibilityPublic = "visibility.Public";
    public final String visibilityConnections = "visibility.Connections";
    public final String editPictureChooseFile = "edit.PictureChooseFile";

    public final String registerButton = "register.Button";

    public final String registerMessageSent = "register.MessageSent";
    public final String registerMessageWrongUsername = "register.MessageWrongUsername";
    public final String registerMessageWrongPassword = "register.MessageWrongPassword";
    public final String registerMessageWrongFirstName = "register.MessageWrongFirstName";
    public final String registerMessageWrongLatsName = "register.MessageWrongLatsName";
    public final String registerMessageWrongPictureVisibility = "register.MessageWrongPictureVisibility";
    public final String registerMessageWrongConfirmPassword = "register.MessageWrongConfirmPassword";

    public final String profileEditChangePassword = "profileEdit.ChangePassword";

    public void fillInAllRequiredFields() {
        Utils.LOG.info("Filling correct data in user registration fields");

        actions.typeValueInField(actions.createRandomString(3) + "@s.com", editUsername);
        actions.typeValueInField(Constants.USER_PASSWORD, editPassword);
        actions.typeValueInField(Constants.USER_PASSWORD, editConfirmPassword);
        actions.typeValueInField(actions.createRandomString(5), editFirstName);
        actions.typeValueInField(actions.createRandomString(5), editLastName);
        actions.clickElement(editPictureVisibility);
        actions.clickElement(visibilityPublic);
        actions.uploadImage(Constants.USER_ONE_PROFILE_IMAGE, editPictureChooseFile);
        actions.clickElement(registerButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    public void fillInAllRequiredFieldsWrongData() {
        Utils.LOG.info("Filling wrong data in user registration fields");
        actions.typeValueInField(actions.createRandomString(5), editConfirmPassword);
        actions.clickElement(registerButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    public ChangePasswordPage openChangePasswordPage() {
        Utils.LOG.info("Opening page for changing password");
        actions.clickElement(profileEditChangePassword);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new ChangePasswordPage();
    }
}

