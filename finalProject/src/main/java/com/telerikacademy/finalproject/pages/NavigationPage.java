package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class NavigationPage extends BasePage {

    public final String logOutButton = "navigation.LogOut";

    public final String usersUsers = "users.Users";

    public final String usersRequests = "users.Requests";
    public final String usersFriends = "users.Friends";

    public final String latestPostsLatestPosts = "latestPosts.LatestPosts";

    public final String latestPostsNewPost = "latestPosts.NewPost";

    public final String profileProfile = "profile.Profile";
    public final String profileEdit = "profile.Edit";

    public final String homeSignIn = "home.SignIn";
    public final String homeSignUp = "home.SignUp";

    public final String settingsCategories = "settings.Categories";
    public final String settingsNationalities = "settings.Nationalities";
    public final String settingsPosts = "settings.Posts";

    public UsersPage openUsersPage() {
        Utils.LOG.info("Opening page with users");
        actions.clickElement(usersUsers);
        actions.implicitWait(Constants.LONG_WAIT);
        return new UsersPage();
    }

    public PostsPage openPostsPage() {
        Utils.LOG.info("opening page with posts");
        actions.clickElement(latestPostsLatestPosts);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new PostsPage();
    }

    public PostCreateEditPage openPostCreatePage() {
        Utils.LOG.info("Opening page for post creation ");
        actions.hoverElement(latestPostsLatestPosts);
        actions.clickElement(latestPostsNewPost);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new PostCreateEditPage();
    }

    public ProfileRegistrationEditPage openRegistrationPage() {
        Utils.LOG.info("Opening page for user registration");
        actions.clickElement(homeSignUp);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new ProfileRegistrationEditPage();
    }

    public ProfileRegistrationEditPage openProfileEditPage() {
        Utils.LOG.info("Opening page for profile edit");
        actions.hoverElement(profileProfile);
        actions.clickElement(profileEdit);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new ProfileRegistrationEditPage();
    }

    public LoginPage openLoginPageAfterLogOut() {
        Utils.LOG.info("Opening login page after clicking logout button");
        actions.clickElement(logOutButton);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new LoginPage();
    }

    public LoginPage openLoginPageAfterSignIn() {
        Utils.LOG.info("Opening login page after clicking sign in button");
        actions.clickElement(homeSignIn);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new LoginPage();
    }

    public UsersPage openFriendsPage() {
        Utils.LOG.info("Opening page with friends");
        actions.hoverElement(usersUsers);
        actions.clickElement(usersFriends);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new UsersPage();
    }

    public UsersPage openRequestOverviewPage() {
        Utils.LOG.info("Opening page overviewing request ");
        actions.hoverElement(usersUsers);
        actions.clickElement(usersRequests);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new UsersPage();
    }

    // Settings page
    public NavigationPage openSettingsCategories() {
        Utils.LOG.info("Opening page with categories");
        actions.hoverElement("settings.Menu");
        actions.clickElement(settingsCategories);
        return new NavigationPage();
    }

    public NavigationPage openSettingsNationalities() {
        Utils.LOG.info("Opening page with nationalities");
        actions.hoverElement("settings.Menu");
        actions.clickElement(settingsNationalities);
        return new NavigationPage();
    }

    public NavigationPage openSettingsPosts() {
        Utils.LOG.info("Opening page with all posts");
        actions.hoverElement("settings.Menu");
        actions.clickElement(settingsPosts);
        return new NavigationPage();
    }

    // STORIES
    public void openMyProfileOverviewPage() {
        Utils.LOG.info("Opening profile page");
        actions.clickElement(profileProfile);
        actions.implicitWait(Constants.SHORT_WAIT);
    }
}