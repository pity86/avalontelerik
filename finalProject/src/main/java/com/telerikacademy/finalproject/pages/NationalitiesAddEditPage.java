package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class NationalitiesAddEditPage extends BasePage{

    public final String nationalityTitle = "nationalityTitle";
    public final String nationalityUpdatedTitle = "nationalityUpdatedTitle";

    public static final String addNationalityButton = Utils.getUIMappingByKey("nationality.Add");
    public static final String nationalityName = Utils.getUIMappingByKey("nationality.Name");
    public static final String saveButton = Utils.getUIMappingByKey("save.Button");
    public static final String nationalityEditButton = Utils.getUIMappingByKey("nationality.Edit");
    public static final String deleteButton = Utils.getUIMappingByKey("delete.deletePostButton");

    public void addNationality() {
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(addNationalityButton);
        Utils.LOG.info("Clicking add button to add new nationality");
        actions.typeValueInField(Constants.NATIONALITY_NAME , nationalityName);
        Utils.LOG.info("Typing nationality name");
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking save button");
        actions.assertElementPresent(nationalityTitle);
    }

    public void editNationality() {
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(nationalityEditButton);
        Utils.LOG.info("Clicking edit button to edit existing nationality");
        actions.clearField(nationalityName);
        actions.typeValueInField(Constants.NATIONALITY_EDIT_NAME , nationalityName);
        Utils.LOG.info("Changing new name of nationality");
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking save button");
        actions.assertElementPresent(nationalityUpdatedTitle);
    }

    public void deleteNationality() {
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(nationalityEditButton);
        Utils.LOG.info("Clicking edit button to edit existing nationality");
        actions.clickElement(deleteButton);
        Utils.LOG.info("Clicking delete button");
        actions.clickElement(saveButton);
        Utils.LOG.info("Confirming deletion of existing nationality");
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }
}
