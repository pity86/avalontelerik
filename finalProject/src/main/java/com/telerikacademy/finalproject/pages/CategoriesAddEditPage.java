package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class CategoriesAddEditPage extends BasePage {

    public final String categoryTitle = "categoryTitle";
    public final String categoryUpdatedTitle = "categoryUpdatedTitle";

    public static final String addCategoryButton = Utils.getUIMappingByKey("categories.Add");
    public static final String categoryFile = Utils.getUIMappingByKey("category.ChooseFile");
    public static final String categoryName = Utils.getUIMappingByKey("category.Name");
    public static final String editCategoryButton = Utils.getUIMappingByKey("categories.Edit");
    public static final String saveButton = Utils.getUIMappingByKey("save.Button");
    public static final String deleteButton = Utils.getUIMappingByKey("delete.deletePostButton");
    public static final String confirmDeletionButton = Utils.getUIMappingByKey("save.Button");

    public void addCategory() {
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(addCategoryButton);
        Utils.LOG.info("Clicking add button to add new category");
        actions.uploadImage(Constants.CATEGORY_NEW_IMAGE, categoryFile);
        Utils.LOG.info("Uploading category image");
        actions.clearField(categoryName);
        actions.typeValueInField(Constants.CATEGORY_NEW_NAME, categoryName);
        Utils.LOG.info("Setting category name");
        actions.clickElement(saveButton);
        Utils.LOG.info("Clicking save button to add new category");
        actions.assertElementPresent(categoryTitle);
    }

    public void editCategory() {
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(editCategoryButton, 1);
        Utils.LOG.info("Clicking edit button to edit existing category");
        actions.uploadImage(Constants.CATEGORY_EDIT_PICTURE, categoryFile);
        Utils.LOG.info("Uploading new image to category");
        actions.clearField(categoryName);
        actions.typeValueInField(Constants.CATEGORY_EDIT_NAME, categoryName);
        Utils.LOG.info("Changing new name of category");
        actions.clickElement(saveButton);
        actions.assertElementPresent(categoryUpdatedTitle);
    }

    public void deleteCategory() {
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(editCategoryButton);
        Utils.LOG.info("Clicking edit button to edit existing category");
        actions.clickElement(deleteButton);
        Utils.LOG.info("Clicking delete button");
        actions.clickElement(confirmDeletionButton);
        Utils.LOG.info("Confirming deletion of existing category");
    }
}
