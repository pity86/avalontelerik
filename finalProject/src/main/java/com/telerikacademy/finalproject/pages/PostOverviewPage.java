package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class PostOverviewPage extends BasePage {

    CommonPage commonPage = new CommonPage();

    public final String latestPost = "post.LatestPost";
    public final String updatedComment = "commentUpdated";

    public final String latestComment = "comment.Edit";
    public final String commentField = "commentEdit.NewComment";
    public final String deleteCommentButton = "delete.deleteCommentButton";
    public final String saveButton = "save.Button";

    public final String commentEditNewComment = "commentEdit.NewComment";

    public void addNewComment(String comment) {
        Utils.LOG.info("Adding new comment to a post");
        actions.typeValueInField(comment, commentEditNewComment);
        actions.clickElement(commonPage.saveButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    public void editComment() {
        actions.implicitWait(Constants.MEDIUM_WAIT);
        actions.clickElement(latestPost);
        Utils.LOG.info("Clicking on latest post");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(latestComment);
        Utils.LOG.info("Clicking on latest comment edit button");
        actions.clearField(commentField);
        actions.typeValueInField(Constants.COMMENT_UPDATE, commentField);
        Utils.LOG.info("Typing new comment content");
        actions.clickElement(saveButton);
        Utils.LOG.info("Saving new comment");
        actions.assertElementPresent(updatedComment);
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }

    public void deleteComment() {
        actions.implicitWait(Constants.MEDIUM_WAIT);
        actions.clickElement(latestPost);
        Utils.LOG.info("Clicking on latest post");
        actions.implicitWait(Constants.SHORT_WAIT);
        actions.clickElement(latestComment);
        Utils.LOG.info("Clicking on latest comment edit button");
        actions.clickElement(deleteCommentButton);
        Utils.LOG.info("Clicking delete comment button");
        actions.clickElement(saveButton);
        Utils.LOG.info("Deleting comment");
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }
}
