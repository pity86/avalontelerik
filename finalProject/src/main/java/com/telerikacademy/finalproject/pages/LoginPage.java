package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class LoginPage extends BasePage {

    public final String loginUsername = "login.Username";
    public final String loginPassword = "login.Password";
    public final String loginButton = "login.Button";
    public final String loginForgotPassword = "login.ForgotPassword";
    public final String loginWrongUsernameOrPassword = "login.WrongUsernameOrPassword";

    public NavigationPage login(String username, String password) {
        Utils.LOG.info("Filling login credentials");
        actions.typeValueInField(username, loginUsername);
        actions.typeValueInField(password, loginPassword);
        actions.clickElement(loginButton);
        actions.implicitWait(Constants.MEDIUM_WAIT);
        return new NavigationPage();
    }

    public ForgotPasswordPage openForgotPasswordPage() {
        Utils.LOG.info("Opening Forgot password page");
        actions.clickElement(loginForgotPassword);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new ForgotPasswordPage();
    }
}
