package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class ForgotPasswordPage extends BasePage {

    public final String forgotPassEmail = "forgotPass.Email";
    public final String forgotPassSend = "forgotPass.Send";

    public final String forgotPassMessageSent = "forgotPass.MessageSent";
    public final String forgotPassUsernameNotExist = "forgotPass.UsernameNotExist";

    public ForgotPasswordPage fillAndSendEmail(String forgotEmail) {
        Utils.LOG.info("Forgot email filling fields ");
        actions.typeValueInField(forgotEmail, forgotPassEmail);
        actions.clickElement(forgotPassSend);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new ForgotPasswordPage();
    }
}
