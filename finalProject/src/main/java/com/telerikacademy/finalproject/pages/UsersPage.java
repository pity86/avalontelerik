package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class UsersPage extends BasePage {

    public final String usersMoreButton = "users.MoreButton";

    public void loadMoreUsers() {
        Utils.LOG.info("Clicking button for showing more users");
        actions.clickElement(usersMoreButton);
        actions.implicitWait(Constants.MEDIUM_WAIT);
    }

    public ProfileOverviewPage openProfileOverviewPage(String usernameKey) {
        Utils.LOG.info("Opening page overviewing profile");
        actions.clickElement(usernameKey);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new ProfileOverviewPage();
    }

    public void findUser(String username) {
        for (int i = 0; i < 10; i++) {
            if (actions.assertElementIsPresentOnPage(username)) {
                break;
            }
            loadMoreUsers();
        }
    }
}

