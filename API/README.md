For successfull execution of the tests you will need the following : 

1. Postman - download and install from https://www.postman.com/downloads/
2. NodeJS - download and install from https://nodejs.org/en/ , after the installation is completed open cmd and type "node -v" to confirm that you have it installed
3. Newman - open cmd and type "npm install -g newman"
4. HTMLExtra reporter - open cmd and type "npm install -g newman-reporter-htmlextra"
5. Xmysql - open cmd and type "npm install -g xmysql"

After installation of the tools mentioned above, run "script.bat" and wait for the tests to be executed. Results can be seen in the html file in folder "newman".